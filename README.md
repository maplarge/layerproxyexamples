# Layer Proxy Examples

This repo holds examples developed using /example.html to demonstrate the functionality of the layer proxy. The initial set of examples were developed using ArcGIS on qa-sandbox.maplarge.com.

## Requirements

The sampes rely on the ACLED data set being loaded into the test account. 

Additionally, the qa-sandbox PKI cert is required if you want to run these examples on that server. Talk to Eric for the certificates.

## Examples


**layerProxyExample-createLayer.html** - This is our basic example. It dynamically creates a layer on the server by posting a JSON object to the API controller then using the resulting layer hash to load the layer via the WMS layer proxy.

**layerProxyExample-changePointSize.html** - Extending the previous example, this example introduces a point size selector on the map that will allow the viewer to dynamically change the size of the dots. This demonstrates how layers are updated via the API controller and then loaded via the WMS layer proxy.

**layerProxyExample-filter.html** - Extending again from the basic createLayer example, this example shows how filtering of data is accomplished on the server via the API controller and updated to the map via the WMS layer proxy.

**layerProxyExample-existingLayer.html** - This example assumes that you have manually saved a layer on the server and want to view it in another application. Replace the value of mlLayerHash with the layer hash of your layer.

**layerProxyExample-temporalWMS.html** - This is a basic implementation of how to use our temporal WMS feature. It clearly shows how to include the temporalWMS object in the layer JSON and how to retrieve a time range of data on the layer.

**layerProxyExample-temporalWMS-step.html** - This example builds on the first example by implementing a stepping function that will walk through the ACLED data in 2 month steps from Jan 2000 to Dec 2015. The stepping function runs automatically and is performs well if you have an adequate internet connection.

## Running the examples

To run the examples, copy and paste the full HTML from the file into the code window on /example.html and then press run.